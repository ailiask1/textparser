package pack.parser;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class Parser {

    final static Logger logger = LogManager.getLogger();

    public String parse() throws IOException {
        logger.info("Start parsing.");
        BufferedReader bufferedReader = new BufferedReader(new FileReader("src\\main\\resources\\input.txt"));
        StringBuilder stringBuilder = new StringBuilder();
        try {
            String line = bufferedReader.readLine();
            while (line != null) {
                logger.debug("Adding new line.");
                stringBuilder.append(line);
                stringBuilder.append(System.lineSeparator());
                line = bufferedReader.readLine();
            }
        } finally {
            bufferedReader.close();
        }
        logger.info("Text was got from input.txt file.");
        return stringBuilder.toString();
    }
}
