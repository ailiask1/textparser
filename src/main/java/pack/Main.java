package pack;

import pack.entity.Text;
import pack.parser.Parser;

import java.io.IOException;

public class Main {

    public static void main(String[] args) throws IOException {
        Parser parser = new Parser();
        String document = parser.parse();
        Text text = new Text(document);
        String sortedText = text.print();
        System.out.println(sortedText);
    }
}
