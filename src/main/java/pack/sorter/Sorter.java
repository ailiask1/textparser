package pack.sorter;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import pack.entity.Paragraph;
import pack.entity.Sentence;
import pack.entity.Word;

import java.util.Collections;
import java.util.List;

public class Sorter {

    final static Logger logger = LogManager.getLogger();

    public List<Paragraph> sortParagraphs(List<Paragraph> paragraphs) {
        for (int i = 0; i < paragraphs.size(); i++) {
            for (int j = 1; j < paragraphs.size(); j++) {
                if (paragraphs.get(j - 1).getSize() > paragraphs.get(j).getSize()) {
                    logger.debug("Swapping paragraphs.");
                    Collections.swap(paragraphs, j - 1, j);
                }
            }
        }
        return paragraphs;
    }

    public List<Sentence> sortSentences(List<Sentence> sentences) {
        for (int i = 0; i < sentences.size(); i++) {
            for (int j = 1; j < sentences.size(); j++) {
                if (sentences.get(j - 1).getSize() > sentences.get(j).getSize()) {
                    logger.debug("Swapping sentences.");
                    Collections.swap(sentences, j - 1, j);
                }
            }
        }
        return sentences;
    }

    public List<Word> sortWords(List<Word> words) {
        int j;
        for (int i = 0; i < words.size(); i++) {
            j = 0;
            while (j < words.size()) {
                if (words.get(i).getSize() < words.get(j).getSize()) {
                    if (!words.get(i).isSentenceElement() && !words.get(j).isSentenceElement()) {
                        logger.debug("Swapping words.");
                        Collections.swap(words, i, j);
                    }
                }
                j++;
            }
        }
        return words;
    }
}
