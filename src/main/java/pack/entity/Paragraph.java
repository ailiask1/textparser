package pack.entity;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import pack.sorter.Sorter;

import java.text.BreakIterator;
import java.util.ArrayList;
import java.util.List;

public class Paragraph implements Element {

    final static Logger logger = LogManager.getLogger();
    private String paragraph;
    private List<Sentence> sentences = new ArrayList<>();

    public Paragraph(String paragraph) {
        this.paragraph = paragraph;
        breakUpParagraph();
    }

    private void breakUpParagraph() {
        BreakIterator breakIterator = BreakIterator.getSentenceInstance();
        breakIterator.setText(paragraph);
        int index = 0;
        while (breakIterator.next() != BreakIterator.DONE) {
            String sentence = paragraph.substring(index, breakIterator.current());
            logger.info("Adding new sentence.");
            sentences.add(new Sentence(sentence));
            index = breakIterator.current();
        }
        String lastSentence = sentences.get(sentences.size() - 1).getSentence();
        if (lastSentence.charAt(lastSentence.length() - 1) == '\r') {
            logger.info("Removing spaces from last sentence.");
            lastSentence = lastSentence.substring(0, lastSentence.length() - 1) + " ";
        }
        logger.info("Changing last sentences.");
        sentences.set(sentences.size() - 1, new Sentence(lastSentence));
    }

    public int getSize() {
        return sentences.size();
    }

    public String print() {
        if (sentences.size() > 1) {
            Sorter sorter = new Sorter();
            logger.debug("Sorting sentences.");
            sentences = sorter.sortSentences(sentences);
        }
        StringBuilder newText = new StringBuilder();
        for (Sentence sentence : sentences) {
            newText.append(sentence.print());
        }
        return newText.toString();
    }
}
