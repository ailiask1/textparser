package pack.entity;

import pack.sorter.Sorter;

import java.text.BreakIterator;
import java.util.ArrayList;
import java.util.List;

public class Sentence implements Element {

    private String sentence;
    private List<Word> words = new ArrayList<>();

    public Sentence(String sentence) {
        this.sentence = sentence;
        breakUpSentence();
    }

    public String getSentence() {
        return sentence;
    }

    public int getSize() {
        String[] wordList = sentence.split("\\s+");
        return wordList.length;
    }

    private void breakUpSentence() {
        BreakIterator breakIterator = BreakIterator.getWordInstance();
        breakIterator.setText(sentence);
        int index = 0;
        while (breakIterator.next() != BreakIterator.DONE) {
            String word = sentence.substring(index, breakIterator.current());
            words.add(new Word(word));
            index = breakIterator.current();
        }
    }

    public String print() {
        Sorter sorter = new Sorter();
        words = sorter.sortWords(words);
        StringBuilder newText = new StringBuilder();
        for (Word word : words) {
            newText.append(word.print());
        }
        return newText.toString();
    }
}

