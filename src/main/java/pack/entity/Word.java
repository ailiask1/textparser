package pack.entity;

public class Word implements Element {

    private String word;

    public Word(String word) {
        this.word = word;
    }

    public int getSize() {
        return word.length();
    }

    public boolean isSentenceElement() {
        String[] sentenceElements = {" ", ",", ".", "'", "?", "!", "\n", "\r"};
        boolean isElement = false;
        for (String sentenceElement : sentenceElements) {
            if (word.contains(sentenceElement)) {
                isElement = true;
                break;
            }
        }
        return isElement;
    }

    public String print() {
        return word;
    }
}
