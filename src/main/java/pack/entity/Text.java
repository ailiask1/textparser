package pack.entity;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import pack.sorter.Sorter;

import java.util.ArrayList;
import java.util.List;

public class Text implements Element {

    final static Logger logger = LogManager.getLogger();
    private String text;
    private List<Paragraph> paragraphList = new ArrayList<>();

    public Text(String text) {
        this.text = text;
        breakUpText();
    }

    private void breakUpText() {
        String[] paragraphs = text.split("\n {2}");
        while (true) {
            int i = 0;
            if (paragraphs[0].charAt(i) != ' ') {
                break;
            }
            logger.debug("Removing spaces of first sentence.");
            paragraphs[0] = paragraphs[0].substring(i + 1);
        }
        for (int i = 0; i < paragraphs.length - 1; i++) {
            logger.info("Adding new paragraph.");
            paragraphList.add(new Paragraph(paragraphs[i]));
        }
        String paragraph = paragraphs[paragraphs.length - 1];
        while (true) {
            if (paragraph.charAt(paragraph.length() - 1) == '\n' || paragraph.charAt(paragraph.length() - 1) == '\r') {
                logger.debug("Removing spaces.");
                paragraph = paragraph.substring(0, paragraph.length() - 2);
            }
            else {
                logger.info("Adding last paragraph.");
                paragraphList.add(new Paragraph(paragraph));
                break;
            }
        }
    }

    public String print() {
        Sorter sorter = new Sorter();
        logger.debug("Sorting paragraphs.");
        paragraphList = sorter.sortParagraphs(paragraphList);
        StringBuilder newText = new StringBuilder();
        for (Paragraph paragraph : paragraphList) {
            newText.append("  ");
            newText.append(paragraph.print());
            newText.append("\n");
        }
        return newText.toString();
    }
}
